

Installation on Raspberry Pi (Buster) with Edge TPU (as client):
===
1. Install Raspberry OS
2. Clone this repository
3. Run `git submodule update --init --recursive` to load `project-posenet`
4. Install requirements by running `code/project-posenet/install_requirements.sh`
5. Install the `libedgetpu1-std` (standard frequency), or `libedgetpu1-max` by follow these instructions:
   https://coral.ai/software/#debian-packages
6. Make sure you have Python >3.7
7. Make a virtual environment (optional, but recommended):
    1. Create a new venv: `python3 -m venv venv/` (creates a venv in `venv/`)
    2. Activate it: `source venv/bin/activate`
8. Install requirements: `pip install -r requirements.txt`
9. You might need to install `PyCoral API`: `pip3 install --extra-index-url https://google-coral.github.io/py-repo/ pycoral`
    * PyCoral version should be >= 1.0.1 ( In case it's not availble in the repo download the latst whl (cp37-arm for Raspberry Pi)  from [here](https://github.com/google-coral/pycoral/releases))
    * If it says something like error like this:
      `Could not install packages due to an EnvironmentError: 404 Client Error: Not Found for url: https://pypi.org/simple/pycoral/`,
      just update pip (`pip install --upgrade pip`) and re-do the step
10. You might to additionally install some system-libs (as of 2021-02):
    `sudo apt install libedgetpu-std libopenjp2-7 libtiff5 libopenexr-dev libavcodec58 libswscale5 libgtk-3-0 liblapack3 libatlas3-base`
    * of course this might change with newer versions, so please use your brain.
11. cd into `code/posenet/` and run `python main.py` to test the installation.
12. Use `python main.py --help` to look at the options, by default it uses the webcam.

Running Server
===
The server code is inside `app/server`. The settings for the database and all Django configuration is inside `server/settings.py`.
Install the requirements using `pip` command and do the following:
1. Run `python3 manage.py migrate` (just for the first run)
2. Run `python3 manage.py runserver` (always)

The API definition of the server is stored as [PostMan](https://www.postman.com/) json file inside the code folder.

Notes
===
Running prerecorded videos: 
At the moment, only two "wall-side" videos recorded on 25-January (with three people playing) work with the library. 
The names in the sciebo folder are: 
GMT20210125-162350_Andr---Mer_1280x720_flipped.mp4
GMT20210125-161515_Andr---Mer_1280x720_flipped.mp4

The branch "uniqueposes" attempts some sort of basic moving average with labeling unique poses but is not accurate.

The `client.py` structure is ready, but it needs completion in order to work with the server.

