from rest_framework import mixins, viewsets
from rest_framework.decorators import api_view
from rest_framework.response import Response

from .models import Client, Pose, Settings
from .serializers import ClientSerializer, PoseSerializer
from .utils import get_system_status, should_calibrate


@api_view(['GET'])
def get_status(resquest):
    status = get_system_status().name
    data = { "system_status": status }

    if 'calibrate' in resquest.query_params and status == "NOT_CALIBRATED": # initiate calibration if not calibrated
        s, created = Settings.objects.get_or_create(key="do_calibration")
        s.value = True
        s.save()
        data["start_calibration"] = True

    return Response(data)

# TODO: The matching function doesn't still work properly. it needs to be merged here.
@api_view(['POST'])
def get_person(request):
    return Response({ "camera": "CAM01", "person": "left" })

class PoseViewSet(
    mixins.CreateModelMixin,
    mixins.ListModelMixin,
    mixins.RetrieveModelMixin,
    viewsets.GenericViewSet): 
    """
    REST API for getting/saving poses.
    """
    serializer_class = PoseSerializer
    
    def get_queryset(self):
        from_datetime = self.request.query_params.get('from', None)
        to_datetime = self.request.query_params.get('to', None)

        qs = Pose.objects.all();

        if from_datetime:
            qs = qs.filter(created_at__gte=from_datetime)
        
        if to_datetime:
            qs = qs.filter(created_at__lte=to_datetime)
        
        return qs

class ClientViewSet(
    mixins.CreateModelMixin,
    viewsets.GenericViewSet):
    """
    ViewSet for starting and closing clients
    """
    serializer_class = ClientSerializer
    queryset = Client.objects.all()

    def create(self, request, *args, **kwargs):
        r = super().create(request, args, kwargs)
        r.data['do_calibration'] = should_calibrate()
        return r        
