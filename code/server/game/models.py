from django.db import models


class Client(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    last_update = models.DateTimeField(auto_now=True)
    calibrated = models.BooleanField(default=False, blank=True)
    name = models.CharField(max_length=255)
    address = models.CharField(max_length=20)

class Pose(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    
    time = models.DateTimeField(db_index=True)
    score = models.FloatField()
    create_by = models.ForeignKey("game.Client", on_delete=models.SET_NULL, null=True)

    nose_x = models.DecimalField(max_digits=20, decimal_places=16)
    nose_y = models.DecimalField(max_digits=20, decimal_places=16)
    nose_score = models.FloatField()

    left_eye_x = models.DecimalField(max_digits=20, decimal_places=16)
    left_eye_y = models.DecimalField(max_digits=20, decimal_places=16)
    left_eye_score = models.FloatField()

    right_eye_x = models.DecimalField(max_digits=20, decimal_places=16)
    right_eye_y = models.DecimalField(max_digits=20, decimal_places=16)
    right_eye_score = models.FloatField()

    left_ear_x = models.DecimalField(max_digits=20, decimal_places=16)
    left_ear_y = models.DecimalField(max_digits=20, decimal_places=16)
    left_ear_score = models.FloatField()

    right_ear_x = models.DecimalField(max_digits=20, decimal_places=16)
    right_ear_y = models.DecimalField(max_digits=20, decimal_places=16)
    right_ear_score = models.FloatField()

    left_shoulder_x = models.DecimalField(max_digits=20, decimal_places=16)
    left_shoulder_y = models.DecimalField(max_digits=20, decimal_places=16)
    left_shoulder_score = models.FloatField()

    right_shoulder_x = models.DecimalField(max_digits=20, decimal_places=16)
    right_shoulder_y = models.DecimalField(max_digits=20, decimal_places=16)
    right_shoulder_score = models.FloatField()

    left_elbow_x = models.DecimalField(max_digits=20, decimal_places=16)
    left_elbow_y = models.DecimalField(max_digits=20, decimal_places=16)
    left_elbow_score = models.FloatField()

    right_elbow_x = models.DecimalField(max_digits=20, decimal_places=16)
    right_elbow_y = models.DecimalField(max_digits=20, decimal_places=16)
    right_elbow_score = models.FloatField()

    left_wrist_x = models.DecimalField(max_digits=20, decimal_places=16, db_index=True)
    left_wrist_y = models.DecimalField(max_digits=20, decimal_places=16, db_index=True)
    left_wrist_score = models.FloatField()

    right_wrist_x = models.DecimalField(max_digits=20, decimal_places=16)
    right_wrist_y = models.DecimalField(max_digits=20, decimal_places=16)
    right_wrist_score = models.FloatField()

    left_hip_x = models.DecimalField(max_digits=20, decimal_places=16)
    left_hip_y = models.DecimalField(max_digits=20, decimal_places=16)
    left_hip_score = models.FloatField()

    right_hip_x = models.DecimalField(max_digits=20, decimal_places=16)
    right_hip_y = models.DecimalField(max_digits=20, decimal_places=16)
    right_hip_score = models.FloatField()

    left_knee_x = models.DecimalField(max_digits=20, decimal_places=16)
    left_knee_y = models.DecimalField(max_digits=20, decimal_places=16)
    left_knee_score = models.FloatField()

    right_knee_x = models.DecimalField(max_digits=20, decimal_places=16)
    right_knee_y = models.DecimalField(max_digits=20, decimal_places=16)
    right_knee_score = models.FloatField()

    left_ankle_x = models.DecimalField(max_digits=20, decimal_places=16)
    left_ankle_y = models.DecimalField(max_digits=20, decimal_places=16)
    left_ankle_score = models.FloatField()

    right_ankle_x = models.DecimalField(max_digits=20, decimal_places=16)
    right_ankle_y = models.DecimalField(max_digits=20, decimal_places=16)
    right_ankle_score = models.FloatField()

class TouchGuess(models.Model):
    """
    All the touch mappings to the board which is based on a specific pose and assigned to a person
    on a specific camera.
    """
    class PlayerPlace(models.TextChoices):
        LEFT = 'L',
        RIGHT = 'R'

    pose = models.ForeignKey(Pose, on_delete=models.CASCADE)
    x = models.DecimalField(max_digits=20, decimal_places=16)
    y = models.DecimalField(max_digits=20, decimal_places=16)
    player = models.CharField(max_length=1, choices=PlayerPlace.choices)
    create_by = models.ForeignKey("game.Client", on_delete=models.SET_NULL, null=True) # redundant but needed.

class Settings(models.Model):
    """
    Save system settings
    """
    key = models.CharField(max_length=32, null=False)
    value = models.CharField(max_length=256)