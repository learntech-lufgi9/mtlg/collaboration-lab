from django.contrib import admin
from .models import Pose, TouchGuess

# Register your models here.
admin.site.register(Pose)
admin.site.register(TouchGuess)