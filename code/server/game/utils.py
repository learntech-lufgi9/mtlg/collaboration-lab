from datetime import datetime, timedelta
from enum import Enum

from django.core.exceptions import ObjectDoesNotExist

from .models import Client, Settings


class SystemStatus(Enum):
    READY = 1
    WAITING_1_CAM = 2
    WAITING_2_CAM = 3
    NOT_CALIBRATED = 4

def get_system_status():
    """
    Gets the status of system based on connected clients
    """
    timeout = datetime.now() - timedelta(seconds=30) # timeout is 30 seconds
    active_cams = Client.objects.filter(last_update__gte=timeout)
    active_cams_count = active_cams.count()

    if active_cams_count == 0:
        return SystemStatus.WAITING_2_CAM
    elif active_cams_count == 1:
        return SystemStatus.WAITING_1_CAM
    else: # 2 cams conncted
        active_cams_calibrated = active_cams.filter(calibrated=True).count() == 2
        if active_cams_calibrated:
            return SystemStatus.RECORDING
        return SystemStatus.CALIBRATING

def get_client_ip(request):
    """
    Get current client's IP Address
    """
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip

def get_current_cam(request):
    """
    Get current camera based on the request IP Address
    """
    ip = get_client_ip(request)
    try:
        return Client.objects.get(address=ip)
    except ObjectDoesNotExist:
        return None

def should_calibrate():
    s, created = Settings.objects.get_or_create(key="do_calibration")
    if created or s.value != 'True':
        return None
    return s.value
