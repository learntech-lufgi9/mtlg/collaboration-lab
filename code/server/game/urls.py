from django.urls import path
from rest_framework.routers import DefaultRouter

from .views import ClientViewSet, PoseViewSet, get_person, get_status

router = DefaultRouter()
router.register(r'pose', PoseViewSet, '/')
router.register(r'client', ClientViewSet, '/')

urlpatterns = [
    path(r'status', get_status),
    path(r'person', get_person)
] + router.urls
