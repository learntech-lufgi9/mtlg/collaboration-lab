from rest_framework import serializers
from datetime import datetime

from .models import Pose, Client, Settings
from .utils import get_current_cam, get_client_ip


class PoseSerializer(serializers.ModelSerializer):
    class Meta:
        model = Pose
        fields = '__all__'
        read_only_fields = [ 'created_by' ]
    
    def create(self, validated_data):
        cam = get_current_cam(self.context.get("request"))
        return Pose.objects.create(**validated_data, create_by=cam)

class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = '__all__'
        read_only_fields = [ 'address' ]
    
    def create(self, validated_data):
        ip = get_client_ip(self.context.get("request"))
        obj, created = Client.objects.get_or_create(**validated_data, address=ip)
        if not created:
            obj.last_update = datetime.now()
        
        return obj
    



