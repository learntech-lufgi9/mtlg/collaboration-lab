#!/usr/bin/env python3
"""Main program to do pose estimation on a given video-file."""
import argparse
import collections
import time
from pathlib import Path
from typing import Generator
from PIL import Image
from coral_engine import Engine, annotate_frame, Keypoint, KeypointType
from calibration import Calibration
import cv2
import numpy as np


cursor_x, cursor_y = 0, 0


def mouse_callback(event, x, y, flags, param):
  global cursor_x, cursor_y
  cursor_x = x
  cursor_y = y


def avg_fps_counter(window_size):
  """FPS counter generator. Copied from project-posenet/pose_camera.py"""
  window = collections.deque(maxlen=window_size)
  prev = time.monotonic()
  yield 0.0  # First fps value.

  while True:
    curr = time.monotonic()
    window.append(curr - prev)
    prev = curr
    yield len(window) / sum(window)


def render_fps(frame: np.ndarray, fps_counter: Generator, inference_time: float) -> np.ndarray:
  """Render the fps(from PoseNet inference-time) and TrueFPS (actual FPS) in the top-right corner."""
  font = cv2.FONT_HERSHEY_PLAIN
  font_scale = 2
  font_thickness = 1
  h, w = frame.shape[:2]
  text = f"FPS: {1000/inference_time:.1f}, TrueFPS: {next(fps_counter):.1f}"
  (text_width, text_height), baseline = cv2.getTextSize(text, font, fontScale=font_scale, thickness=font_thickness)
  padding = 2
  frame = cv2.rectangle(frame, (w-text_width-padding, 0), (w, text_height+baseline+padding), color=(0, 0, 0),
                        thickness=cv2.FILLED, lineType=cv2.LINE_AA)  # background
  return cv2.putText(frame, text, (w-text_width, (text_height+baseline)),
                     font, font_scale, (255, 255, 255), font_thickness, cv2.LINE_AA)


def map_obj2game(obj_point, game_width, game_height, scale):
  """Maps the object coordinates to the game-screen coordinates.
  Here we make a lot of assumption of the calibration and game-screen:
  * the world-origin (bottom-right corner of recognized chessboard pattern) is at (2,2) in game-coordinates
  * the recognized chessboard pattern does not cover the whole game-screen:
    - i.e. recognized pattern is (n_rows=13, n_cols=6) (# of where the black squares touch)
    - the overall game pattern is (16, 9). Together with the origin-offset we compute the actual coords.
  """
  obj_point = [obj_point[0] + 2, obj_point[1] + 2]  # fix origin offset=2
  game_point = [game_width - obj_point[0], game_height - obj_point[1]]
  game_point = (int(game_point[0] * scale), int(game_point[1] * scale))
  return tuple(game_point)


def draw_shaded_circle(frame, point, radius, mode="gray"):
  """Draws a shaded circle where the center is the brightest."""
  assert mode == "gray"
  for r in range(radius, -1, -1):
    frame = cv2.circle(frame, point, radius=r, color=255*(1 - r/radius))
  return frame


def render_game(frame, poses, calibration, debug=True):
  """Renders a small field representative of the game in the top-right corner."""
  important_kps = []
  for pose_idx, pose in enumerate(poses):
    for kp_item in [KeypointType.LEFT_WRIST, KeypointType.RIGHT_WRIST]:
      kp = pose.keypoints[kp_item]
      if kp.score < 0.1:  # skip those with very low prob
        continue
      important_kps.append((kp, pose_idx))

  game_scale = 15
  game_width = calibration.n_cols + 1
  game_height = calibration.n_rows + 3
  bg_game = np.zeros(shape=(game_height * game_scale, game_width * game_scale), dtype='uint8')

  for (keypoint, pose_num) in important_kps:
    keypoint: Keypoint
    pt = keypoint.point
    obj_point = calibration.map_image2obj((pt.x, pt.y))
    game_point = map_obj2game(obj_point, game_width, game_height, scale=game_scale)
    bg_game = draw_shaded_circle(bg_game, game_point, radius=8, mode="gray")
    # We do not necessarily need this, but for sanity check, we do it anyways:
    # we convert the object-coordinates back to the image-screen.
    # Assuming we have done everything correctly,
    # this will match (almost) exactly the original screen coordinates.
    # this is slightly different from `calibration.obj2img`,
    # since this does not take the distortion into account (we have already undistorted the frame).
    img_pts, jacobian = cv2.projectPoints(obj_point, calibration.r_vecs, calibration.t_vecs,
                                          calibration.camera_matrix, None)
    frame = cv2.circle(frame, tuple(np.int32(img_pts).ravel()), radius=4, color=(255, 0, 0), thickness=1)

  if debug:
    obj_point = calibration.map_image2obj((cursor_x, cursor_y))
    game_point = map_obj2game(obj_point, game_width, game_height, scale=game_scale)
    bg_game = draw_shaded_circle(bg_game, game_point, radius=8, mode="gray")

  bg_game = Image.fromarray(bg_game)
  bg_game = bg_game.convert('RGB')
  frame[:game_height*game_scale, :game_width*game_scale] = bg_game  # top-left corner
  return frame


def main():
  global cursor_x, cursor_y
  """Do the main-loop over a video, including control&init stuff."""
  parser = argparse.ArgumentParser(description="Does human pose estimation on a given video or camera.")
  parser.add_argument("--video_src", action="store", default=None, help="Video source: if not given, will use camera.")
  parser.add_argument("--save_to", action="store", default=None, help="Destination for annotated video.")
  parser.add_argument("--headless", action="store_true", default=False, help="Running in headless mode, without showing windows.")
  args = parser.parse_args()

  # webcam by default, index 0.
  video_src = 0 if args.video_src is None else args.video_src
  cap = cv2.VideoCapture(video_src)
  if not cap.isOpened():
    print(f"Cannot open video source '{video_src}'")
    return
  width = cap.get(cv2.CAP_PROP_FRAME_WIDTH)
  height = cap.get(cv2.CAP_PROP_FRAME_HEIGHT)
  fps = cap.get(cv2.CAP_PROP_FPS)
  num_frames = cap.get(cv2.CAP_PROP_FRAME_COUNT)
  delay = 1000/fps
  print(f"Reading video with FPS={fps}, delay={delay:.3f} ms, {num_frames} frames.")
  print(f"Video dimensions: {width:.0f}x{height:.0f}")

  engine = Engine()
  fps_counter = avg_fps_counter(30)
  need_resize = (engine.req_width != width) or (engine.req_height != height)
  print(f"Requires resizing? {need_resize}")
  # skip_to = 240
  # cap.set(cv2.CAP_PROP_POS_FRAMES, skip_to)

  video_writer = None
  if args.save_to is not None:
    save_to = Path(args.save_to).expanduser()
    assert save_to.parent.is_dir()
    assert not save_to.exists(), "Not replacing video file, delete first!"
    print(f"Saving to '{str(save_to)}'")
    video_writer = cv2.VideoWriter(str(save_to), cv2.VideoWriter_fourcc(*"mp4v"), fps,
                                   (engine.req_width, engine.req_height))

  calibration = Calibration((13, 6), min_frames=10, discard_frames=1)

  show_debug_frame = 50  # for how many frames the game plane should be shown.
  debug = {"image2game": False,
           "calibration": True,
           "posenet_keypoints": False
           }

  if not args.headless:
    cv2.namedWindow("frame")
    if debug["image2game"]:
      cv2.setMouseCallback("frame", mouse_callback)

  while True:
    ret, frame = cap.read()
    if not ret:
      print("Can't receive frame (stream end?). Exiting ...")
      break
    frame: np.ndarray
    if calibration.is_calibrated:
      frame = calibration.undistort_frame(frame)
    else:
      frame = calibration.perform_calibration(frame, debug=debug["calibration"])
    frame: Image = engine.maybe_resize(frame)  # resize to model size
    poses, inference_time = engine.process_frame(frame)
    frame = np.array(frame)
    frame: np.ndarray = annotate_frame(frame, poses, debug=debug["posenet_keypoints"])
    frame = render_fps(frame, fps_counter, inference_time)
    if calibration.is_calibrated:  # after posenet-processing
      if debug["calibration"] and show_debug_frame > 0:
        show_debug_frame -= 1
        frame = calibration.draw_game_plane(frame, after_undistortion=True)
        frame = calibration.draw_axes(frame, after_undistortion=True)
      frame = render_game(frame, poses, calibration, debug=debug["image2game"])
    if not args.headless:
      cv2.imshow('frame', frame)
    if video_writer is not None:
      video_writer.write(frame)
    key_pressed = cv2.waitKey(1)
    if key_pressed == ord('q'):
      break
    elif key_pressed == ord('n'):
      cap.set(cv2.CAP_PROP_POS_FRAMES, cap.get(cv2.CAP_PROP_POS_FRAMES) + int(2*fps))  # skip two seconds
    elif key_pressed == ord('p'):
      cap.set(cv2.CAP_PROP_POS_FRAMES, max(0, cap.get(cv2.CAP_PROP_POS_FRAMES) - int(2 * fps)))  # rewind two seconds
  if video_writer is not None:
    video_writer.release()
  cap.release()
  cv2.destroyAllWindows()


if __name__ == "__main__":
  import better_exchook
  better_exchook.install()
  main()
