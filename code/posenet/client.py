import argparse

import requests

"""
Functions
"""
def send_get(url):
    """
    Send GET API request and return json parsed result if successful
    """
    r = requests.get(url)
    if r.status_code == 200:
        return r.json()
    return r.content


def send_post(url, data):
    """
    Send POST API request and return json parsed result if successful
    """
    r = requests.post(url, data=data)
    if r.status_code == 200:
        return r.json()
    return r.content

def beacon(server, name, calibrated=False):
    """
    Register this client to the server and checks status.
    Returns True if calibration is required, false otherweise
    """
    data = send_post(server + "/client", {
        "calibrate": calibrated,
        "name": name
    })

    if not calibrated and "calibrated" in data: # json data
        if data["calibrated"]:
            return True
    return False


"""
Main function
"""
def main():
    parser = argparse.ArgumentParser(description="Human pose estimation on a given video or camera (Press q to exit)")
    parser.add_argument("server", metavar='SERVER', help="Server address with protocol and without trailing /. E.g. http://127.0.0.1:8000")
    parser.add_argument("name", metavar='NAME', help="Name of the client camera which is used in predicted player's name.")
    parser.add_argument("--video_src", action="store", default=None, help="Video source: if not given, will use first camera.")

    args = parser.parse_args()

    server = args.server
    name = args.name

    # webcam by default, index 0.
    video_src = 0 if args.video_src is None else args.video_src
    cap = cv2.VideoCapture(video_src)
    if not cap.isOpened():
        print(f"Cannot open video source '{video_src}'")
        return
    width = cap.get(cv2.CAP_PROP_FRAME_WIDTH)
    height = cap.get(cv2.CAP_PROP_FRAME_HEIGHT)
    fps = cap.get(cv2.CAP_PROP_FPS)
    num_frames = cap.get(cv2.CAP_PROP_FRAME_COUNT)
    delay = 1000/fps
    print(f"Reading video with FPS={fps}, delay={delay:.3f} ms, {num_frames} frames.")
    print(f"Video dimensions: {width:.0f}x{height:.0f}")


    # Calibration
    calibration = Calibration((13, 6), min_frames=10, discard_frames=1)

    # Main Loop
    while True:
        ret, frame = cap.read()
        if not ret:
            print("Can't receive frame (stream end?). Exiting ...")
            break
        frame: np.ndarray
        if calibration.is_calibrated:
            frame = calibration.undistort_frame(frame)
        else:
            frame = calibration.perform_calibration(frame, debug=False)
        frame: Image = engine.maybe_resize(frame)  # resize to model size
        poses, inference_time = engine.process_frame(frame)

        # TODO: Send poses to server
        frame = np.array(frame)
        frame = render_fps(frame, fps_counter, inference_time)
        if calibration.is_calibrated:  # after posenet-processing
            frame = render_game(frame, poses, calibration, debug=False)
        if key_pressed == ord('q'):
            break

    if video_writer is not None:
        video_writer.release()
    cap.release()

if __name__ == '__main__':
    main()
    