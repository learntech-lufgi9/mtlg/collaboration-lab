"""
Operates the edge TPU and produces pose estimations.
For installation instructions, see
https://coral.ai/docs/accelerator/get-started/#pycoral-on-linux

Most of this code has been copied from `project-posenet/pose_engine.py`.
"""
import collections
import enum
import math
import os
import time
from typing import List, Tuple

import cv2
import numpy as np
from PIL import Image
from pycoral.utils import edgetpu
from tflite_runtime.interpreter import load_delegate
from tflite_runtime.interpreter import Interpreter

EDGETPU_SHARED_LIB = 'libedgetpu.so.1'
POSENET_SHARED_LIB = os.path.join('posenet_lib', os.uname().machine, 'posenet_decoder.so')

# DEFAULT_MODEL = "models/mobilenet/posenet_mobilenet_v1_075_481_641_quant_decoder_edgetpu.tflite"
DEFAULT_MODEL = "models/mobilenet/posenet_mobilenet_v1_075_721_1281_quant_decoder_edgetpu.tflite"
# DEFAULT_MODEL = "models/resnet/posenet_resnet_50_960_736_32_quant_edgetpu_decoder.tflite"


class KeypointType(enum.IntEnum):
  """Pose keypoints."""
  NOSE = 0
  LEFT_EYE = 1
  RIGHT_EYE = 2
  LEFT_EAR = 3
  RIGHT_EAR = 4
  LEFT_SHOULDER = 5
  RIGHT_SHOULDER = 6
  LEFT_ELBOW = 7
  RIGHT_ELBOW = 8
  LEFT_WRIST = 9
  RIGHT_WRIST = 10
  LEFT_HIP = 11
  RIGHT_HIP = 12
  LEFT_KNEE = 13
  RIGHT_KNEE = 14
  LEFT_ANKLE = 15
  RIGHT_ANKLE = 16


EDGES = (
    (KeypointType.NOSE, KeypointType.LEFT_EYE),
    (KeypointType.NOSE, KeypointType.RIGHT_EYE),
    (KeypointType.NOSE, KeypointType.LEFT_EAR),
    (KeypointType.NOSE, KeypointType.RIGHT_EAR),
    (KeypointType.LEFT_EAR, KeypointType.LEFT_EYE),
    (KeypointType.RIGHT_EAR, KeypointType.RIGHT_EYE),
    (KeypointType.LEFT_EYE, KeypointType.RIGHT_EYE),
    (KeypointType.LEFT_SHOULDER, KeypointType.RIGHT_SHOULDER),
    (KeypointType.LEFT_SHOULDER, KeypointType.LEFT_ELBOW),
    (KeypointType.LEFT_SHOULDER, KeypointType.LEFT_HIP),
    (KeypointType.RIGHT_SHOULDER, KeypointType.RIGHT_ELBOW),
    (KeypointType.RIGHT_SHOULDER, KeypointType.RIGHT_HIP),
    (KeypointType.LEFT_ELBOW, KeypointType.LEFT_WRIST),
    (KeypointType.RIGHT_ELBOW, KeypointType.RIGHT_WRIST),
    # (KeypointType.LEFT_HIP, KeypointType.RIGHT_HIP),
    # (KeypointType.LEFT_HIP, KeypointType.LEFT_KNEE),
    # (KeypointType.RIGHT_HIP, KeypointType.RIGHT_KNEE),
    # (KeypointType.LEFT_KNEE, KeypointType.LEFT_ANKLE),
    # (KeypointType.RIGHT_KNEE, KeypointType.RIGHT_ANKLE),
)

Point = collections.namedtuple('Point', ['x', 'y'])
Point.distance = lambda a, b: math.sqrt((a.x - b.x) ** 2 + (a.y - b.y) ** 2)
Point.distance = staticmethod(Point.distance)

Keypoint = collections.namedtuple('Keypoint', ['point', 'score'])

Pose = collections.namedtuple('Pose', ['keypoints', 'score'])


def annotate_frame(frame: Image, poses: List[Pose], debug=False) -> np.ndarray:
  """Annotates a frame with the given pose information."""
  threshold_value = 0.6
  frame = np.array(frame)
  keypoints = []
  edges = []
  for pose in poses:
    for key, keypoint in pose.keypoints.items():
      keypoint: Keypoint
      key_x, key_y = keypoint.point.x, keypoint.point.y
      if keypoint.score < threshold_value:
        continue
      keypoints.append(cv2.KeyPoint(key_x, key_y, keypoint.score))
    for a, b in EDGES:
      if a not in pose.keypoints or b not in pose.keypoints:
        continue
      a_key = pose.keypoints[a]
      b_key = pose.keypoints[b]
      if a_key.score < threshold_value or b_key.score < threshold_value:
        continue
      edges.append([(a_key.point.x, a_key.point.y), (b_key.point.x, b_key.point.y)])
  if debug:
    frame = cv2.drawKeypoints(frame, keypoints=keypoints, outImage=np.array([]),
                              flags=cv2.DRAW_MATCHES_FLAGS_NOT_DRAW_SINGLE_POINTS)
  for pt1, pt2 in edges:
    frame = cv2.line(frame, pt1, pt2, color=(255, 255, 255), thickness=2, lineType=cv2.LINE_AA)
  return frame


class Engine:
  """Operates the TFLite and EdgeTPU interfacing."""
  def __init__(self, model=None):
    if model is None:
      model = DEFAULT_MODEL
    self.model = model
    print("Initializing EdgeTPU engine...", end="")
    try:
      edgetpu_delegate = load_delegate(EDGETPU_SHARED_LIB)
    except ValueError as e:
      raise ValueError("%s\nCheck that:\n* the Edge TPU is connected\n* `libedgetpu-std` is installed." % e)
    posenet_decoder_delegate = load_delegate(POSENET_SHARED_LIB)
    self._interpreter = Interpreter(
      self.model, experimental_delegates=[edgetpu_delegate, posenet_decoder_delegate])
    self._interpreter.allocate_tensors()
    print("ok!")
    input_shape = self.get_input_tensor_shape()
    print("input shape:", input_shape)
    assert len(input_shape) == 4  # [batch, height, width, channels]
    assert input_shape[0] == 1 and input_shape[-1] == 3
    self.req_height = input_shape[1]
    self.req_width = input_shape[2]
    self._input_type = self._interpreter.get_input_details()[0]['dtype']

  def maybe_resize(self, frame: Image) -> Image:
    """Resize the given image to the size required by the model."""
    if isinstance(frame, np.ndarray):
      frame = Image.fromarray(frame.astype('uint8'), 'RGB')
    resized_image = frame.resize((self.req_width, self.req_height), Image.NEAREST)
    return resized_image

  def process_frame(self, frame: Image) -> Tuple[List[Pose], float]:
    """Run the frame through posenet inference and parse its output."""
    input_data = np.expand_dims(frame, axis=0)  # [B, H, W, C]
    if self._input_type is np.float32:
      input_data = np.float32(input_data) / 128.0 - 1.0  # rescale to [-1, 1] for posenet
    else:
      input_data = np.asarray(input_data)  # assumes uint8
    start = time.monotonic()
    edgetpu.run_inference(self._interpreter, input_data.flatten())
    inference_time = (time.monotonic() - start) * 1000  # in milliseconds
    return self.parse_output(), inference_time

  def get_input_tensor_shape(self):
    """Returns input tensor shape."""
    return self._interpreter.get_input_details()[0]['shape']

  def get_output_tensor(self, idx):
    """Returns output tensor view."""
    return np.squeeze(self._interpreter.tensor(
      self._interpreter.get_output_details()[idx]['index'])())

  def parse_output(self) -> List[Pose]:
    """Parses interpreter output tensors and returns decoded poses."""
    keypoints = self.get_output_tensor(0)
    keypoint_scores = self.get_output_tensor(1)
    pose_scores = self.get_output_tensor(2)
    num_poses = self.get_output_tensor(3)
    poses = []
    for i in range(int(num_poses)):
      pose_score = pose_scores[i]
      pose_keypoints = {}
      for j, point in enumerate(keypoints[i]):
        y, x = point
        pose_keypoints[KeypointType(j)] = Keypoint(
          Point(x, y), keypoint_scores[i, j])
      poses.append(Pose(pose_keypoints, pose_score))
    return poses
