"""Quick test of opencv's CLAHE on images in same folder."""
# import cv2
# #From https://stackoverflow.com/questions/24341114/simple-illumination-correction-in-images-opencv-c 
# #-----Reading the image-----------------------------------------------------
# img = cv2.imread('wallside.jpg', 1)
# cv2.imshow("img",img)
# 
# gray1 = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
# cv2.imshow("orig to gray", gray1)
# 
# #-----Converting image to LAB Color model----------------------------------- 
# lab= cv2.cvtColor(img, cv2.COLOR_BGR2LAB)
# #cv2.imshow("lab",lab)
# 
# #-----Splitting the LAB image to different channels-------------------------
# l, a, b = cv2.split(lab)
# #cv2.imshow('l_channel', l)
# #cv2.imshow('a_channel', a)
# #cv2.imshow('b_channel', b)
# 
# #-----Applying CLAHE to L-channel-------------------------------------------
# clahe = cv2.createCLAHE(clipLimit=3.0, tileGridSize=(8,8))
# cl = clahe.apply(l) 
# cv2.imshow('CLAHE output', cl)
# 
# #-----Merge the CLAHE enhanced L-channel with the a and b channel-----------
# limg = cv2.merge((cl,a,b))
# #cv2.imshow('limg', limg)
# 
# #-----Converting image from LAB Color model to RGB model--------------------
# final = cv2.cvtColor(limg, cv2.COLOR_LAB2BGR)
# cv2.imshow('final', final)
# 
# gray = cv2.cvtColor(final, cv2.COLOR_RGB2GRAY)
# cv2.imshow('returned', gray)
# cv2.waitKey()
# #_____END_____#


"""From https://stackoverflow.com/questions/61121763/how-to-remove-glare-from-images-in-opencv""" 
import cv2
import numpy as np

# read image
img = cv2.imread('new_windowside.png') 

# convert to gray
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

# threshold grayscale image to extract glare
mask = cv2.threshold(gray, 220, 255, cv2.THRESH_BINARY)[1]

# Optionally add some morphology close and open, if desired
#kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (7,7))
#mask = cv2.morphologyEx(mask, cv2.MORPH_CLOSE, kernel, iterations=1)
#kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (3,3))
#mask = cv2.morphologyEx(mask, cv2.MORPH_OPEN, kernel, iterations=1)


# use mask with input to do inpainting
result = cv2.inpaint(img, mask, 21, cv2.INPAINT_TELEA) 

# write result to disk
cv2.imwrite("side_mask.png", mask)
cv2.imwrite("side_inpaint.png", result)

# display it
cv2.imshow("IMAGE", img)
cv2.imshow("GRAY", gray)
cv2.imshow("MASK", mask)
cv2.imshow("RESULT", result)
cv2.waitKey(0)