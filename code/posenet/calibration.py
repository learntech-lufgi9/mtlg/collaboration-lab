"""Implements the camera calibration procedure. Uses OpenCV to do that."""
from typing import Tuple
import cv2
import numpy as np
from PIL import Image


class Calibration:
  """
  Find the chess board corners
  Note, currently the whole grid is not recognized (15, 9), but this works for camera calibration.
  TODO: remove glares by using CLAHE/black-white or some other method
  """
  def __init__(self, grid_size: Tuple[int, int], min_frames: int = 10, discard_frames: int = 5):
    assert isinstance(grid_size, tuple) and len(grid_size) == 2
    self.n_cols, self.n_rows = grid_size[0], grid_size[1]
    self.discard_frames = discard_frames  # # we discard the first few frames (auto-focus/adjust)
    self.min_frames = min_frames + self.discard_frames
    self.pattern_points = np.zeros((self.n_cols * self.n_rows, 3), np.float32)
    self.pattern_points[:, :2] = np.mgrid[0:self.n_cols, 0:self.n_rows].T.reshape(-1, 2)
    self.obj_points = []
    self.img_points = []
    self.roi = None
    self.camera_matrix = None
    self.new_camera_matrix = None
    self.dist_coeffs = None
    self.is_calibrated = False
    self.map1, self.map2 = None, None
    self.r_vecs, self.t_vecs = None, None
    self.game_plane = None  # four corners of the game-plane in object coordinates

  def perform_calibration(self, frame: Image, debug=False):
    """Tries to detect the pattern in the given frame. May trigger a camera calibration"""
    gray = cv2.cvtColor(frame, cv2.COLOR_RGB2GRAY)
    ret, corners = cv2.findChessboardCorners(gray, (self.n_cols, self.n_rows),
                                             flags=cv2.CALIB_CB_NORMALIZE_IMAGE | cv2.CALIB_CB_ADAPTIVE_THRESH |
                                             cv2.CALIB_CB_FAST_CHECK)
    if not ret:
      return frame
    criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 100, 0.001)
    corners2 = cv2.cornerSubPix(gray, corners, (11, 11), (-1, -1), criteria)
    self.obj_points.append(self.pattern_points)
    self.img_points.append(corners2)
    if debug:
      print(f"Found chessboard pattern, got {len(self.img_points)} frames, need {self.min_frames}.")
      frame = cv2.drawChessboardCorners(frame, (self.n_cols, self.n_rows), corners2, ret)

    if len(self.obj_points) >= self.min_frames:
      self.calibrate_camera(gray)
    return frame

  def draw_axes(self, frame, after_undistortion=True):
    """Draws axes in the origin of the found coordinate-system."""
    axis = np.float32([[0, 0, 0], [3, 0, 0], [0, 3, 0], [0, 0, -3]]).reshape(-1, 3)
    imgpts = self.obj2img(axis, after_undistortion)
    corner = tuple(imgpts[0].ravel())
    frame = cv2.line(frame, corner, tuple(imgpts[1].ravel()), (255, 0, 0), 5)
    frame = cv2.line(frame, corner, tuple(imgpts[2].ravel()), (0, 255, 0), 5)
    frame = cv2.line(frame, corner, tuple(imgpts[3].ravel()), (0, 0, 255), 5)
    return frame

  def draw_game_plane(self, frame, after_undistortion):
    """Draws a plane over the whole game.
    This currently assumes that the chessboard that was found (e.g. 13x6) is in the middle to the actual chessboard.
    So now we have "padding" of two and can compute the whole game-area."""
    axis = np.float32([[-2, -2, 0], [-2, self.n_rows+1, 0], [self.n_cols+1, self.n_rows+1, 0], [self.n_cols+1, -2, 0]])
    axis: np.ndarray
    imgpts = self.obj2img(axis, after_undistortion)
    imgpts = np.int32(imgpts).reshape(1, -1, 2)
    frame = cv2.polylines(frame, imgpts, isClosed=True, color=(255, 255, 0), thickness=2, lineType=cv2.LINE_AA)
    self.game_plane = axis
    return frame

  def map_image2obj(self, img_pos):
    """This functions maps the image-coordinates to game-coordinates.
    Since this transformation is ill-posed, we add the constraint that the object-position has z=0.
    Assumes that the frame (of the image-positions) is undistorted.
    Basically we invert the pinhole equations.
    Idea from:
    https://stackoverflow.com/questions/12299870/computing-x-y-coordinate-3d-from-image-point
    """
    assert self.is_calibrated
    rotation_mat = cv2.Rodrigues(self.r_vecs)[0]
    inv_rotation_mat = np.linalg.inv(rotation_mat)
    inv_proj_matrix = np.linalg.inv(self.camera_matrix)
    translation_vec = self.t_vecs[:, 0]  # w.r.t world origin (table)

    uv_point = np.array([img_pos[0], img_pos[1], 1])  # in image-coordinates, f=1
    left_side_mat = inv_proj_matrix @ uv_point
    left_side_mat = inv_rotation_mat @ left_side_mat
    right_side_mat = inv_rotation_mat @ translation_vec

    z = 0  # assumes that world-coordinate is on the table
    s = (z + right_side_mat[2]) / left_side_mat[2]
    obj_point = (s * inv_proj_matrix @ uv_point) - translation_vec
    obj_point = inv_rotation_mat @ obj_point
    return obj_point

  def obj2img(self, obj_points, after_undistortion=False):
    assert self.is_calibrated
    img_points, jacobian = cv2.projectPoints(obj_points, self.r_vecs, self.t_vecs, self.camera_matrix, self.dist_coeffs)
    if after_undistortion:
      img_points = cv2.undistortPoints(img_points, self.camera_matrix, self.dist_coeffs, None, self.new_camera_matrix)
    return img_points

  def calibrate_camera(self, frame):
    """Performs the actual calibration to get the camera matrix."""
    assert len(frame.shape) == 2, "requires gray-scale image"
    assert len(self.obj_points) >= self.min_frames
    assert len(self.obj_points) == len(self.img_points)

    h, w = frame.shape[:2]
    ret, self.camera_matrix, self.dist_coeffs, self.r_vecs, self.t_vecs = cv2.calibrateCamera(
      self.obj_points[self.discard_frames:],
      self.img_points[self.discard_frames:],
      frame.shape[::-1], None, None)
    self.r_vecs = np.mean(self.r_vecs, axis=0)  # only keep last
    self.t_vecs = np.mean(self.t_vecs, axis=0)
    if not ret:
      return
    self.new_camera_matrix, self.roi = cv2.getOptimalNewCameraMatrix(self.camera_matrix, self.dist_coeffs, (w, h),
                                                                     alpha=0, newImgSize=(w, h))
    self.map1, self.map2 = cv2.initUndistortRectifyMap(self.camera_matrix, self.dist_coeffs, None,
                                                       self.new_camera_matrix, (w, h), m1type=cv2.CV_32FC1)
    self.is_calibrated = True

  def undistort_frame(self, frame: np.ndarray) -> np.ndarray:
    """
    Performs the actual undistortion of a frame
    by applying the remapping.

    :param frame:
    :return: the undistorted frame
    """
    assert self.is_calibrated
    dst = cv2.remap(frame, self.map1, self.map2, cv2.INTER_LINEAR)
    x, y, w, h = self.roi
    dst = dst[y:y + h, x:x + w]
    return dst
