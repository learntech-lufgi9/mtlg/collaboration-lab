% This file provides an example Beamer presentation using the RWTH theme
% showcasing some of the more common options, similar to the Powerpoint version
% 12.11.2014: Revision 1 (Harold Bruintjes, Tim Lange)
% 23.10.2015: Edit by Kai Frantzen

% For RWTH, beamer should be loaded with class option t (top)
\documentclass[t]{beamer}
\usepackage[utf8]{inputenc}
\usepackage{amssymb}
\usepackage{pifont}
\usepackage{multirow}
\usepackage{booktabs}
\usepackage{hyperref}
 
% German style date formatting (footer)
\usepackage[ddmmyyyy]{datetime}
\renewcommand{\dateseparator}{.}

% Format the captions used for figures etc.
\usepackage[compatibility=false]{caption}
\captionsetup{singlelinecheck=off,justification=raggedleft,labelformat=empty,labelsep=none}

\usetheme{rwth}
\graphicspath{{figures/}}

% https://tex.stackexchange.com/questions/426088/texlive-pretest-2018-beamer-and-subfig-collide
\makeatletter
\let\@@magyar@captionfix\relax
\makeatother

% https://tex.stackexchange.com/questions/83440/inputenc-error-unicode-char-u8-not-set-up-for-use-with-latex
% https://gist.github.com/beniwohli/798549
%\DeclareUnicodeCharacter{25A0}{\ding{110}}
\DeclareUnicodeCharacter{25A0}{$\blacksquare$}  % or \textbullet

\newcommand{\insertframenumbering}{\insertframenumber{} of \inserttotalframenumber}
\newcommand{\fitin}[1]{\parbox{0ex}{\mbox{#1}}}
\setbeamercolor{framesubtitle}{fg=black}
\addtobeamertemplate{frametitle}{}{%
  \ifx\insertframesubtitle\@empty\else%
  \usebeamerfont{framesubtitle}%
  \usebeamercolor[fg]{framesubtitle}%
  \insertframesubtitle%
  \fi%
}

% https://tex.stackexchange.com/questions/2541/beamer-frame-numbering-in-appendix
\newcommand{\backupbegin}{
   \newcounter{framenumberappendix}
   \setcounter{framenumberappendix}{\value{framenumber}}
}
\newcommand{\backupend}{
   \addtocounter{framenumberappendix}{-\value{framenumber}}
   \addtocounter{framenumber}{\value{framenumberappendix}} 
}

\newcommand{\TODO}{{\color{red} TODO {}}}
% https://tex.stackexchange.com/questions/200838/how-do-i-make-dots-more-compact-or-are-there-alternatives
\newcommand{\smalldots}{\makebox[0.8em][c]{...}}


 
% Setup presentation information
\title{Collaboration Research Lab}
\subtitle{André Merboldt, Valerie Tan, Abdolali Faraji}
\date[RWTH]{\today}
\author[Max]{Max Musterman mmusterman@cs.rwth-aachen.de}
\institute[]{RWTH Aachen University}

% Set the logo to the file `logo`
\logo{\includegraphics{rwth_hltpr_bild_rgb.jpg}}

% Uncomment this if you want a TOC at every section start
%\AtBeginSection{\frame{
%    \frametitle{Content}
%    \tableofcontents[currentsection]
%}}

%\usefonttheme[onlymath]{serif}

% Use this to control some aspects of the footer
%\setbeamertemplate{footertextextra}{Extra text in the footer \enskip{}Extra text in the footer}
%\setbeamertemplate{footertext}{ title | name | institut | date | Example of an overridden footer (insert name, institut, date, etc.)}
\setbeamertemplate{footertext}{ Collaboration Research Lab | Learning Technologies i9}

\begin{document}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Title page with a 2/3rd size picture
\setbeamertemplate{title page}[rwth2][title_large]{}
\begin{frame}[plain]
\titlepage
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\section{Problem description}
\begin{frame}{Problem statement}
\vfill
Situation:
\begin{itemize}
\item Multi-touch device with multiple users
\item Device does not tell us the identity of the user who interacted
\end{itemize}
\vfill
Goal
\begin{itemize}
\item Mapping of \textbf{touch interaction to user identity}
\item \textbf{Without interfering} the participants
\item Somewhat efficient (\textbf{real-time} capable)
\end{itemize}
\vfill
\end{frame}

\section{Our approach}
\begin{frame}{Possible approaches}
\vfill
\begin{enumerate}
\item \textbf{Detect pose of each participant using pose estimation}
\begin{itemize}
\item Using two RGB cameras at above-head level
\item Preferable on the long side of the table (better pose estimation)
\end{itemize}
\vfill
\item Depth-enabled detection
\begin{itemize}
\item Mount a depth camera on the ceiling
\item Perform some kind of ``image segmentation''
\begin{itemize}
\item each pixel $\to$ \{background, user1, user2, \smalldots\}
\end{itemize}
\end{itemize}
\vfill
\item Hand/Arm Gesture Detection
\begin{itemize}
\item Then to which person belongs this hand?
\end{itemize}
\end{enumerate}
\vfill
\end{frame}

\begin{frame}{Pose estimation}
    \begin{center}
        \includegraphics[width=\textwidth]{mpv-shot0001.jpg}
    \end{center}
\end{frame}


\begin{frame}{Pose estimation approach}
\begin{columns}[onlytextwidth]\
% Text on the left
\begin{column}{.6\textwidth}
Details:
\begin{itemize}
\item Estimation is done on Edge computing devices
\end{itemize}
\vspace{1cm}
Challenges:
\begin{itemize}
\item \textbf{Mapping} 2D key-points \\to 3D room-coordinates \\to 2D game positions
\item \textbf{Calibration of camera} positions
\begin{itemize}
\item We don't want to input the exact camera-positions each time
\item But we need them for the mapping!
\end{itemize}
\item Pose estimation is \textbf{computationally expensive}
\begin{itemize}
\item Even on ML chips
\begin{itemize}
\item ($\sim$13 FPS on Edge TPU...\textbf{with USB2.0!})
\end{itemize}
\end{itemize}
\end{itemize}
\end{column}
\begin{column}{.4\textwidth}
\raisebox{-10cm}[0pt][10cm]{\includegraphics[height=10cm]{approach_collab_lab_birdview}}
\end{column}
\end{columns}
\end{frame}

\section{Mapping from 2D key-points to 3D positions}
\begin{frame}
\begin{columns}[T]
\begin{column}{0.5\linewidth}
In general the 2D ``wrist'' key-point may lay on any point on the ray in 3D.\\
\vspace{1ex}
However, since we know the 3D location/dimension of the table,
we can infer the \textbf{intersection between the ray and the table}.\\
$\to$ Then we can check whether an actual touch happened.\\
\vspace{2ex}
\begin{center}
% https://ps.is.tuebingen.mpg.de/publications/jmartinez-iccv-2017
% "A simple yet effective baseline for 3d human pose estimation"
\includegraphics[width=0.8\textwidth]{{Screen_Shot_2017-08-09_at_12.54.00.png}}
\footnotesize\\Source: 
\href{https://ps.is.tuebingen.mpg.de/publications/jmartinez-iccv-2017}{https://ps.is.tuebingen.mpg.de/publications/jmartinez-iccv-2017}
\end{center}
\end{column}
\begin{column}{0.5\textwidth}
\begin{itemize}
\item For this we need the camera calibration (distance, rotation, intrinsic camera settings)
\item Should happen on a the server, which combines information:
\begin{itemize}
    \item \textbf{temporal pose} information
    \item \textbf{touch interactions} on the device
\end{itemize}
\vspace{2ex}
\item Accuracy improvements:
\begin{itemize}
    \item Estimate finger tip position (crop image)
    \begin{itemize}
        \item by looking at keypoints elbow+wrist
        \item by using hand gesture detection (OpenCV)
    \end{itemize}
\end{itemize}
\vspace{2ex}
\item Other option would be to use temporal correlations
\begin{itemize}
    \item VideoPose3D (by facebook research)
\end{itemize}
\end{itemize}
\end{column}
\end{columns}
\end{frame}



\begin{frame}
\begin{center}
\includegraphics[height=\textheight]{sequence_diagram_interface}
\end{center}
\end{frame}


%where to put these slides?
\section{Dimensions}
\begin{frame}{}
\begin{center}
\includegraphics[width=\textwidth]{lab_dimensions_init} 
\end{center}
\end{frame}

\begin{frame}{}
\begin{center}
\includegraphics[width=\textwidth]{side_dimensions} 
\end{center}
\end{frame}

\section{Hardware Recommendations}
\begin{frame}{}
\begin{center}
\begin{itemize}
    \item \textbf{Coral Edge TPU} works well with Raspberryi Pi ($\sim$13 FPS with 480p on Rpi3)
    \item \textbf{Jetson Nano}:
    \begin{itemize}
    % TODO: mention cropping, rescaling. FPS (~16 in some github issue?)
        \item NVIDIA Official human pose detection network (\texttt{trt\_pose} which is different from project-posenet) has been tested. But we faced a number of issues:
        \begin{itemize}
            \item There is no official GUI for the project but a Jupyter notebook with no extra real-time data like frame-rate.
            \item Network is also tested with our own script (without a notebook). It has better performance, but the GUI part is not handled yet.
            \item The network is trained on low-resolution pictures (244px x 244px). We can either rescale the camera feed or cut it into parts
        \end{itemize}
        \item Maybe use Jetson-nano for other objectives?
    \end{itemize}
    \item \textbf{Intel RealSense d435 Depth Camera}
    \begin{itemize}
        \item Depth value may be helpful for mapping 2D points to 3D positions. Exact recommendation dependent on upcoming sprints. 
        \item Still having multiple technical difficulties with the librealsense. 
    \end{itemize}%%%%% !! TODO (val) 
   
\end{itemize}
\end{center}
\end{frame}

\begin{frame}{Conclusion \& Next Steps}
\vfill
What we have \textcolor{rwth}{done so far}:
\begin{itemize}
    \item Ran pose estimation approach on our videos
    \item Concluded that corner camera does not work well
    \item Setup environment on Rpi \& Jetson Nano
    \item Thought about the approach for converting coordinates
\end{itemize}
\vfill
What we \textcolor{rwth}{need to proceed}:
\begin{itemize}
    \item Recordings for camera calibration (chessboard/QR-codes)
    \item ``Dataset'' of (touch-interactions, video)
\end{itemize}
\vfill
\begin{center}
\textbf{\textcolor{rwth}{\Huge Any questions/comments/suggestions?}}
\end{center}
\end{frame}

% Final frame, subtext is optional
% Note: should be plain
\setbeamertemplate{final page}[rwth][Any questions?]{Thank you for your attention}
\begin{frame}[plain]
\usebeamertemplate{final page}
\end{frame}

\end{document}
