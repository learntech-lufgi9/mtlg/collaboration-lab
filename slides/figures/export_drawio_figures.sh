#!/bin/bash
# convert *.drawio to *.pdf in current dir
for f in `ls *.drawio`; do
  if [[ "$f" -nt ${f//.drawio}.pdf ]]; then
    drawio --crop --transparent -x -o ${f//.drawio}.pdf $f
  else
    echo "${f//.drawio}.pdf is already up-to-date"
  fi
done

